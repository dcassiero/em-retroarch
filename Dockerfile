# FROM dcassiero/em-dosbox-base:gamething-v1.0
FROM nginx
LABEL maintainer="Daniel Cassiero <daniel.cassiero@gmail.com>"

ENV RETROARCH_VERSION=1.16.0

RUN apt-get update && apt-get install -y p7zip-full coffeescript --no-install-recommends && \
    rm -rf /var/lib/apt/lists/*

ADD retroarch-${RETROARCH_VERSION} /usr/share/nginx/html/
WORKDIR /usr/share/nginx/html/assets/cores
RUN ../../indexer > .index-xhr
WORKDIR /usr/share/nginx/html/assets/frontend/bundle
RUN ../../../indexer > .index-xhr

EXPOSE 80
